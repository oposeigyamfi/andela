﻿using System;
using AndelaProxy.Models;


namespace ClientConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var picture = new PictureObject {Text = "red", ForeGround = "180x79", BgColour = "ffffff"};

            var proxy = new ProxyClient();

            var response = proxy.GetPicture(picture);

            Console.ReadLine();
        }
    }
}

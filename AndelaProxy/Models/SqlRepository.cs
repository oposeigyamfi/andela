﻿
using System.Linq;

namespace AndelaProxy.Models
{
    public class SqlRepository
    {
        public PictureObject GetByText(string text)
        {
            using (var  entity = new AndelaEntities())
            {
                return entity.Picture.Where(x => x.Text.ToLower().Equals(text.ToLower())).Select(x => new PictureObject
                {
                    Text = x.Text,
                    ForeGround = x.ForeColor,
                    BgColour = x.BgColor

                }).FirstOrDefault();
            }
        }

        public void InsertPicture(PictureObject model)
        {
            using (var entity = new AndelaEntities())
            {
                entity.Picture.Add(new Picture
                {
                    Text = model.Text,
                    BgColor = model.BgColour,
                    ForeColor = model.ForeGround,
                });

                entity.SaveChanges();
            }
        }
    }
}

﻿using System;
using System.Drawing;
using System.Net.Http;
using System.Configuration;
using System.Drawing.Imaging;
using System.Net.Http.Headers;

namespace AndelaProxy.Models
{
    public class ProxyClient
    {
        private readonly HttpClient _client;
        private readonly SqlRepository _repository;
        private readonly string _path;

        public ProxyClient()
        {
            _client = new HttpClient {
                BaseAddress = new Uri(ConfigurationManager.AppSettings["api-url"])
            };

            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add( new MediaTypeWithQualityHeaderValue("application/json"));

            _repository = new SqlRepository();

            _path = AppDomain.CurrentDomain.BaseDirectory + @"imgs\";
        }

        public Image GetPicture(PictureObject model)
        {
            try
            {
                //get object from object from store exist  
                var foundImage = _repository.GetByText(model.Text);

                if (foundImage != null)
                    return GetImage(model.Text);

                var response = _client.GetAsync(_client.BaseAddress + 
                    "/" + model.ForeGround + "/" + model.BgColour + "?text=" + model.Text).Result;
                
                var stream = response.Content.ReadAsStreamAsync().Result;
                var image = Image.FromStream(stream);

                _repository.InsertPicture( model);
                image.Save(_path + model.Text +".png" , ImageFormat.Png);

                return image;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public Image GetImage(string name) { 
            return Image.FromFile(@_path + name+ ".png");
        }
    }
}
